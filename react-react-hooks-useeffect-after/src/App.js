import React, { useState, useEffect } from "react";

const App = ({ put, remove }) => {
  const [value, setValue] = useState(0);
  const [reset, setReset] = useState(0);

  useEffect(() => {
    put("agilejs:app", "HOOKS");
  }, []);

  useEffect(() => {
    put("agilejs:app:value", value);
    return () => remove("agilejs:app:value");
  });

  useEffect(() => {
    put("agilejs:app:reset", reset);
    return () => remove("agilejs:app:reset");
  }, [reset]);

  return (
    <div>
      <h1>Current value is {value}</h1>
      <h3>Reset executed {reset} times</h3>

      <button onClick={() => setValue(value - 1)}>-</button>
      <button onClick={() => setValue(value + 1)}>+</button>
      <button
        onClick={() => {
          setValue(0);
          setReset(reset + 1);
        }}
      >
        Reset
      </button>
    </div>
  );
};

export default App;
