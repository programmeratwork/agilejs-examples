import React from "react";
import ReactDOM from "react-dom";

import App from "./App";

const localStoragePut = (key, value) => window.localStorage.setItem(key, value);
const localStorageRemove = key => window.localStorage.removeItem(key);

const rootElement = document.getElementById("root");
ReactDOM.render(
  <App put={localStoragePut} remove={localStorageRemove} />,
  rootElement
);
