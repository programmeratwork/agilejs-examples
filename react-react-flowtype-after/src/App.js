import React, { Component } from "react";

type HeaderProps = {
  title: string
};

const Header = ({ title }: HeaderProps) => {
  return (
    <header className="App-header">
      <h1 className="App-title">{title}</h1>
    </header>
  );
};

Header.defaultProps = {
  title: "Default header title"
};

type BodyProps = {
  footer: string
};

type BodyState = {
  count: number
};

class Body extends Component<BodyProps, BodyState> {
  static defaultProps = {
    footer: "Bye bye"
  };

  state = {
    count: 0
  };

  render() {
    return (
      <p className="App-intro">
        To get started, edit <code>src/App.js</code> and save to reload.
        <footer>{this.props.footer}</footer>
      </p>
    );
  }
}

class App extends Component<{}> {
  render() {
    return (
      <div className="App">
        <Header title="header text" />
        <Body />
      </div>
    );
  }
}

export default App;
