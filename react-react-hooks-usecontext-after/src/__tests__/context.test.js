import React from "react";
import { render } from "react-testing-library";

import App from "../App";

test("user login should be available in Header", () => {
  const { getByText } = render(<App />);

  expect(getByText(/borillo/i)).toBeDefined();
});
