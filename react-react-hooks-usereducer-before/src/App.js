import React from "react";

const initialState = { count: 0 };

function reducer(state, action) {}

const App = () => {
  const [state, dispatch] = [null, null];

  return (
    <div>
      <h1>Current value is {state.count}</h1>

      <button onClick={() => dispatch({ type: "DECREMENT" })}>-</button>
      <button onClick={() => dispatch({ type: "INCREMENT" })}>+</button>
    </div>
  );
};

export default App;
