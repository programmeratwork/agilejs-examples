import React from "react";
import ReactDOM from "react-dom";

import "react-testing-library/cleanup-after-each";

import { render, fireEvent } from "react-testing-library";

import App from "../src/App";

test("assert elements using the label", () => {
  const _ = render(<App />);
  let value;

  expect(value).toEqual("borillo");
});

test("assert elements using placeholder", () => {
  const _ = render(<App />);

  expect(value).toEqual("This is a new user");
});

test("assert elements using textual values", () => {
  const _ = render(<App />);

  expect(value).toEqual("New user form");
});
