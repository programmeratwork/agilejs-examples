const registerUser = (name, mail, password) => {
  cy.get("#name").type(name);
  cy.get("#mail").type(mail);
  cy.get("#password").type(password);
  cy.get(".registration-form button").click();
};

context("Registration form", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("show form and fields when init registration process", () => {
    cy.get(".registration-form");
    cy.get("#name");
    cy.get("#mail");
    cy.get("#password").should("have.attr", "type", "password");
    cy.get(".registration-form button");
  });

  it("show alert when registration is over", () => {
    registerUser(
      "Programmer At Work",
      "hello@programmeratwork.com",
      "typehereyourpass"
    );

    cy.get(".registration-form .alert").contains("It works");
  });

  it("show registered users", () => {
    registerUser(
      "Programmer At Work",
      "hello@programmeratwork.com",
      "typehereyourpass"
    );

    cy.get(".users").contains("Programmer At Work");
  });
});
