import React, { Component } from "react";

class App extends Component {
  state = {
    value: ""
  };

  constructor(props) {
    super(props);
  }

  componentDidMount = () => {};

  handleInputChange = event => {
    event.preventDefault();
    this.setState({ value: event.target.value });
  };

  render() {
    return (
      <div>
        <input
          type="text"
          name="user"
          onChange={this.handleInputChange}
          value={this.state.value}
        />
      </div>
    );
  }
}

export default App;
