import axios from "axios";

export default {
  async retrieveDirectors() {
    return await axios({
      url: "/api/directors",
      method: "get",
      responseType: "json"
    });
  }
};
