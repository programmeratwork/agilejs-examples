import axios from "axios";

export default {
  async retrieveFilms() {
    return await axios({
      url: "/api/films",
      method: "get",
      responseType: "json"
    });
  }
};
