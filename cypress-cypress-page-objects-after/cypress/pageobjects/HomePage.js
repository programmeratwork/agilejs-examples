class HomePage {
  constructor(wrapper) {
    this.wrapper = wrapper;
  }

  open() {
    this.wrapper.visit("/");
  }

  title() {
    return this.wrapper.title();
  }

  sectionTitle() {
    return this.wrapper.get(".films-panel");
  }

  menuItems() {
    return this.wrapper.get(".app-menu > li");
  }
}

export default HomePage;
