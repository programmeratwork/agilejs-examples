class FilmsPanel {
  constructor(wrapper) {
    this.wrapper = wrapper;
  }

  open() {
    this.wrapper.server();
    this.wrapper.route("/api/films", "fixture:films");

    this.wrapper.visit("/films");
  }

  films() {
    return this.wrapper.get(".film");
  }

  filmsTitles() {
    return this.wrapper.get(".film .title");
  }

  filmsEpisodes() {
    return this.wrapper.get(".film .episode");
  }
}

export default FilmsPanel;
