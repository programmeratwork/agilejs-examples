import HomePage from "../../pageobjects/HomePage";

context("Home page", () => {
  let homePage;

  beforeEach(() => {
    homePage = new HomePage(cy);
    homePage.open();
  });

  it("should show the app title", () => {
    const title = homePage.title();

    title.should("eq", "ReactWars");
  });

  it("should show the film list as default info", () => {
    const sectionTitle = homePage.sectionTitle();

    sectionTitle.contains("Film list");
  });

  it("should show a menu", () => {
    const menuItems = homePage.menuItems();

    menuItems.should(nodeList => {
      expect(nodeList).to.have.length(2);
    });
  });
});
