import FilmsPanel from "../../pageobjects/FilmsPanel";

context("Films", () => {
  let filmsPanel;

  beforeEach(() => {
    filmsPanel = new FilmsPanel(cy);
    filmsPanel.open();
  });

  it("should show the episode list", () => {
    const films = filmsPanel.films();

    films.should("have.length", 8);
  });

  it("should show the episode name and number", () => {
    const titles = filmsPanel.filmsTitles();
    titles.contains("La amenaza fantasma");

    const episodes = filmsPanel.filmsEpisodes();
    episodes.contains("Episode 1");
  });
});
