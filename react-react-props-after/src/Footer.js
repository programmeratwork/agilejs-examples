import React from "react";
import PropTypes from "prop-types";

const Footer = ({ year }) => {
  return <div>Copyright &copy; All rights reserved {year}</div>;
};

Footer.defaultProps = {
  year: 3000
};

Footer.propTypes = {
  year: PropTypes.number.isRequired
};

export default Footer;
