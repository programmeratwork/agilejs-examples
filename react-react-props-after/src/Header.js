import React, { Component } from "react";
import PropTypes from "prop-types";

class Header extends Component {
  static defaultProps = {
    subtitle: "Default subtitle for Header"
  };

  static propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    onActivate: PropTypes.func
  };

  render() {
    const { title = "Default title for Header", subtitle } = this.props;

    if (this.props.onActivate) {
      this.props.onActivate("Component has been activate!!");
    }

    return (
      <div>
        <span>Logo</span>
        {" | "}
        <span>{title}</span>
        {" | "}
        <span>{subtitle}</span>
      </div>
    );
  }
}

export default Header;
