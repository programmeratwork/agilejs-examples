import React, { Component } from "react";

import Header from "./Header";
import Footer from "./Footer";

const functionProperty = message => {
  console.log(message);
};

class App extends Component {
  render() {
    return (
      <div>
        <Header
          title="Sample props React App"
          subtitle="Detailed usage of properties in React"
          onActivate={functionProperty}
        />
        <Header title="Another title for sample props" />

        <Footer year={2018} />
        <Footer />
      </div>
    );
  }
}

export default App;
