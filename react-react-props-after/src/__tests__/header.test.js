import React from "react";

import "react-testing-library/cleanup-after-each";
import { render } from "react-testing-library";

import Header from "../Header";

test("define header with mandatory title", () => {
  const { getByText } = render(<Header title="The header title" />);

  const title = getByText("The header title");
  expect(title).toBeDefined();
});

test("subtitle should have a default value", () => {
  const { getByText } = render(<Header title="The header title" />);

  const subtitle = getByText("Default subtitle for Header");
  expect(subtitle).toBeDefined();
});

test("define header with subtitle", () => {
  const { getByText } = render(
    <Header title="The header title" subtitle="And this is the subtitle" />
  );

  const subtitle = getByText("And this is the subtitle");
  expect(subtitle).toBeDefined();
});

test("could call activate event", () => {
  const onActivate = jest.fn();
  const { getByText } = render(
    <Header title="The header title" onActivate={onActivate} />
  );

  expect(onActivate).toHaveBeenCalledTimes(1);
});
