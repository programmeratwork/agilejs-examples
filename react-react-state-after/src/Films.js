import React, { Component, Fragment } from "react";
import FilmsRepository from "./FilmsRepository";

const Film = ({ episode, title, year }) => (
  <div>
    {episode} - {title} - {year}
  </div>
);

class Films extends Component {
  state = {
    films: []
  };

  async componentDidMount() {
    const films = await FilmsRepository.retrieveFilms();
    this.setState({ films });
  }

  render() {
    return (
      <Fragment>
        {this.state.films.map(film => (
          <Film {...film} />
        ))}
      </Fragment>
    );
  }
}

export default Films;
