import React, { Component } from "react";

import Films from "./Films";

class App extends Component {
  render() {
    return (
      <div>
        <Films />
      </div>
    );
  }
}

export default App;
