import React from "react";
import renderer from "react-test-renderer";

import { Header } from "../App";

test("header should match snapshot", () => {
  const tree = renderer.create(<Header />).toJSON();
  expect(tree).toMatchSnapshot();
});
