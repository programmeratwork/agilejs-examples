import React, { Component } from "react";

export const Header = () => {
  return (
    <section>
      <header className="header">Main header</header>
      <footer className="footer">Main footer</footer>
    </section>
  );
};

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <h1>App</h1>
      </div>
    );
  }
}

export default App;
