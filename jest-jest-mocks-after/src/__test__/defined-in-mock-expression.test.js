jest.mock(
  "../User",
  () =>
    class {
      register() {
        return "Registered in an inline mock!!";
      }
    }
);

import User from "../User";

test("create mocks inline when defining mocked module", () => {
  const user = new User();

  expect(user.register()).toEqual("Registered in an inline mock!!");
});
