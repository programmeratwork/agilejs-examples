const sampleTestElements = ["item1", "item2", "item3"];

const createTestElement = (attributeName, attributeValue, text) => {
  const element = document.createElement("div");
  element.setAttribute(attributeName, attributeValue);
  element.textContent = text;

  return element;
};

beforeAll(() => {
  sampleTestElements
    .map(label => createTestElement("class", "item", label))
    .forEach(item => document.body.appendChild(item));

  const uniqueElement = createTestElement("id", "unique-element", `I'm unique`);
  document.body.appendChild(uniqueElement);
});

test("should be able to retrieve DOM nodes through `querySelector`", () => {
  const item = document.querySelector("#unique-element");
  expect(item.constructor.name).toEqual("HTMLDivElement"); 
});

test("should be able to retrieve all DOM nodes through `querySelectorAll`", () => {
  const item = document.querySelectorAll(".item");
  expect(item.length).toEqual(sampleTestElements.length);
});
