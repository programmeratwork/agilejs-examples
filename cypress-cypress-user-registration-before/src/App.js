import React, { Component } from "react";

class App extends Component {
  state = {
    alert: null,
    users: []
  };

  constructor(props) {
    super(props);

    this.refName = React.createRef();
    this.refMail = React.createRef();
    this.refPassword = React.createRef();
  }
  register = e => {
    e.preventDefault();

    this.setState(prevState => ({
      alert: "Congratulations!! It works :)",
      users: [
        ...prevState.users,
        {
          name: this.refName.current.value,
          mail: this.refMail.current.value,
          password: this.refPassword.current.value
        }
      ]
    }));
  };

  render() {
    const { alert, users } = this.state;

    return (
      <div>
        <div className="registration-form">
          {alert && <strong className="alert">{alert}</strong>}
          
          <form>
            <div>
              <label htmlFor="name">Name:</label>
              <input id="name" type="text" name="name" ref={this.refName} />
            </div>
            <div>
              <label htmlFor="mail">Mail:</label>
              <input id="mail" type="text" name="mail" ref={this.refMail} />
            </div>
            <div>
              <label htmlFor="password">Password:</label>
              <input
                id="password"
                type="password"
                name="password"
                ref={this.refPassword}
              />
            </div>
            <div>
              <button onClick={this.register}>Register me!!</button>
            </div>
          </form>
        </div>

        <div className="users">
          {users.length > 0 && <h1>Registered user list</h1>}
          {users.map(({ name, mail }) => (
            <div>{`${name} (${mail})`}</div>
          ))}
        </div>
      </div>
    );
  }
}

export default App;
