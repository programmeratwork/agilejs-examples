const STORED_KEY = "test";
const STORED_VALUE = "value";

test("store and retrieve value", () => {
  window.localStorage.setItem(STORED_KEY, STORED_VALUE);
  expect(window.localStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);
});

test("remove item", () => {
  window.localStorage.setItem(STORED_KEY, STORED_VALUE);
  expect(window.localStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);

  window.localStorage.removeItem(STORED_KEY);
  expect(window.localStorage.getItem(STORED_KEY)).toBeFalsy();
});

test("length", () => {
  window.localStorage.setItem(STORED_KEY, STORED_VALUE);
  expect(window.localStorage.length).toEqual(1);
});

test("clear items", () => {
  window.localStorage.setItem(STORED_KEY, STORED_VALUE);
  expect(window.localStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);

  window.localStorage.clear();
  expect(window.localStorage.length).toEqual(0);
});
