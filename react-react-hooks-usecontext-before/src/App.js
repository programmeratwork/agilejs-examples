import React from "react";

const UserContext = React.createContext("user");

const Header = () => {
  const { login } = {};

  return (
    <div>
      <h2>Header with hooks</h2>
      <span>User: {login}</span>
    </div>
  );
};

const Page = () => (
  <div>
    <h1>Page</h1>
    <Header />
  </div>
);

const App = () => {
  return (
    <UserContext.Provider
      value={{
        login: "borillo",
        name: "Ricardo Borillo"
      }}
    >
      <Page />
    </UserContext.Provider>
  );
};

export default App;
