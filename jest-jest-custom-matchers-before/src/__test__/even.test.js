import sum from "../sum";

expect.extend({
  toBeEven(received) {
    return {
      message: () => "Not implemented",
      pass: false
    };
  }
});

test("should detect even numbers", () => {
  expect(sum(4, 2)).toBeEven();
  expect(sum(4, 1)).not.toBeEven();
});
