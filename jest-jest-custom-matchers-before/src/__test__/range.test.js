import sum from "../sum";

const LOWER_LIMIT = 0;
const UPPER_LIMIT = 10;

expect.extend({
  toBeInRange(received, lower, upper) {
    if (received >= lower && received <= upper) {
      return { pass: true };
    }

    return {
      message: () => `expected ${received} to be in range`,
      pass: false
    };
  }
});

test("should be in range", () => {
  expect(sum(1, 2)).toBeInRange(LOWER_LIMIT, UPPER_LIMIT);
  expect(sum(2, 2)).not.toBeInRange(0, 1);
});
