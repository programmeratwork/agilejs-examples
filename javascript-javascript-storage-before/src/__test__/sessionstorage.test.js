const STORED_KEY = "test";
const STORED_VALUE = "value";

test("store and retrieve value", () => {
  expect(window.sessionStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);
});

test("remove item", () => {
  window.sessionStorage.setItem(STORED_KEY, STORED_VALUE);
  expect(window.sessionStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);

  expect(window.sessionStorage.getItem(STORED_KEY)).toBeFalsy();
});

test("length", () => {
  expect(window.sessionStorage.length).toEqual(1);
});

test("clear items", () => {
  window.sessionStorage.setItem(STORED_KEY, STORED_VALUE);
  expect(window.sessionStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);

  expect(window.sessionStorage.length).toEqual(0);
});
