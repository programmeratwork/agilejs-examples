const STORED_KEY = "test";
const STORED_VALUE = "value";

test("store and retrieve value", () => {
  expect(window.localStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);
});

test("remove item", () => {
  window.localStorage.setItem(STORED_KEY, STORED_VALUE);
  expect(window.localStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);

  expect(window.localStorage.getItem(STORED_KEY)).toBeFalsy();
});

test("length", () => {
  expect(window.localStorage.length).toEqual(1);
});

test("clear items", () => {
  window.localStorage.setItem(STORED_KEY, STORED_VALUE);
  expect(window.localStorage.getItem(STORED_KEY)).toEqual(STORED_VALUE);

  expect(window.localStorage.length).toEqual(0);
});
