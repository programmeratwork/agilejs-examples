window.localStorage = window.sessionStorage = {
  _keys: {},
  getItem(key) {
    const value = this._keys[key];
    return typeof value === "undefined" ? null : value;
  },
  setItem(key, value) {
    this._keys[key] = value;
  },
  removeItem(key) {
    let removed = delete this._keys[key];

    return removed;
  },
  clear() {
    this._keys = {};
  },
  get length() {
    return Object.keys(this._keys).length;
  }
};
