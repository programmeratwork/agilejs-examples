import React, { Component, Fragment } from "react";
import AuthRepository from "./AuthRepository";

import Header from "./Header";
import Body from "./Body";
import UserContext from "./UserContext";

class App extends Component {
  state = {
    credentials: {}
  };

  async componentDidMount() {
    const authRepository = new AuthRepository();
    const credentials = await authRepository.retrieveCredentials();

    this.setState({ credentials });
  }

  render() {
    return (
      <UserContext.Provider value={this.state.credentials}>
        <Fragment>
          <Header />
          <Body />
        </Fragment>
      </UserContext.Provider>
    );
  }
}

export default App;
