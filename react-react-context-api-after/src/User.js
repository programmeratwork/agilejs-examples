import React from "react";

import UserContext from "./UserContext";

const User = () => (
  <UserContext.Consumer>
    {({ user }) => (
      <div>
        <strong>Logged as:</strong> <span>{user}</span>
      </div>
    )}
  </UserContext.Consumer>
);

export default User;
