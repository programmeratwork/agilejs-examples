import React from "react";
import { render, cleanup, fireEvent } from "react-testing-library";

import App from "../App";

afterEach(cleanup);

test("initial value should be zero", () => {
  const { getByText } = render(<App />);
  expect(getByText(/current value is 0/i)).toBeDefined();
});

test("should increment when plus button is pressed", () => {
  const { getByText } = render(<App />);
  const plusOneButton = getByText("+");

  fireEvent.click(plusOneButton);

  expect(getByText(/current value is 1/i)).toBeDefined();
});

test("should decrement when minus button is pressed", () => {
  const { getByText } = render(<App />);
  const plusOneButton = getByText("-");

  fireEvent.click(plusOneButton);

  expect(getByText(/current value is -1/i)).toBeDefined();
});
