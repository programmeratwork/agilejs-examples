import User from "../User";

test("create mocks using the __mocks__ directory", () => {
  const user = new User();

  expect(user.register()).toEqual("Registered!!");
});
