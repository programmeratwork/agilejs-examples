import React, { Component } from "react";

class App extends Component {
  constructor(props) {
    super(props);
    this.alreadyBindedSayHi = this.sayHi.bind(this);
  }

  classFunctionSayHi = () => {
    console.log(this.props.message);
  };

  sayHi() {
    console.log(this.props.message);
  }

  render() {
    const globalSayHi = () => console.log(this.props.message);

    return (
      <div>
        <button onClick={() => console.log(this.props.message)}>
          Say Hi!!
        </button>
        <button onClick={globalSayHi}>Say Hi!!</button>
        <button onClick={this.sayHi.bind(this)}>Say Hi!!</button>
        <button onClick={this.alreadyBindedSayHi}>Say Hi!!</button>
        <button onClick={this.classFunctionSayHi}>Say Hi!!</button>
      </div>
    );
  }
}

export default App;
