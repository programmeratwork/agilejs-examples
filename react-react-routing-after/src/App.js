import React, { Component } from "react";
import { Route, Switch, Link } from "react-router-dom";

const Fuji = props => {
  const { awesome } = props.location.state;
  const { color } = props;

  return (
    <div style={{ color }}>
      <span>I'm a Fuji apple!! </span>
      {awesome && <span>And i'm awesome!!</span>}
    </div>
  );
};

const Granny = ({
  match: {
    params: { id }
  },
  color
}) => {
  return (
    <div style={{ color }}>I'm a Granny Smith apple!! And my ID is {id}</div>
  );
};

const Apple = ({ match, location }) => {
  return (
    <div>
      <h1>Welcome to the apple's world!!</h1>

      <nav>
        <Link to={{ pathname: match.url + "/fuji", state: { awesome: true } }}>
          Fuji
        </Link>
        {" | "}
        <Link to={match.url + "/granny/99"}>Granny</Link>
      </nav>

      <div>
        <Switch location={location}>
          <Route
            exact
            path={match.url + "/fuji"}
            render={props => <Fuji {...props} color="red" />}
          />
          <Route
            path={match.url + "/granny/:id"}
            render={props => <Granny {...props} color="blue" />}
          />
          <Route render={props => <span>Ops!! not an apple :/</span>} />
        </Switch>
      </div>
    </div>
  );
};

const Orange = () => <div>No subtypes for orange have been defined :/</div>;

class App extends Component {
  render() {
    return (
      <div>
        <h1>Food Market</h1>

        <nav>
          <Link to="/apple">Apple</Link>
          {" | "}
          <Link to="/orange">Orange</Link>
        </nav>

        <div>
          <Switch>
            <Route path="/apple" component={Apple} />
            <Route path="/orange" component={Orange} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
