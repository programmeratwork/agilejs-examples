import React, { Component } from "react";

class App extends Component {
  render() {
    return (
      <div>
        <pre>{`
            Create this component structure: 
            
            <div>
              <Header />
              <Body />
              <Footer />
            </div>
        `}</pre>
      </div>
    );
  }
}

export default App;
