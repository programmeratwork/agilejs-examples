test("Access symbol's `description` without calling `toString`", () => {
  const testSymbol = Symbol("Orange");

  expect(testSymbol.description).toEqual("Orange");
});
