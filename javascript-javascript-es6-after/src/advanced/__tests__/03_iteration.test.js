const fruits = ["Mango", "Orange"];

const fruitIterator = list => {
  let index = 0;
  return {
    next: () => {
      if (index >= list.length) return { done: true };

      return { value: list[index++], done: false };
    }
  };
};

test("`next` interface for iterators", () => {
  const iterator = fruitIterator(fruits);

  expect(iterator.next().value).toEqual("Mango");
  expect(iterator.next().value).toEqual("Orange");
  expect(iterator.next().value).not.toBeDefined();
});

const idGenerator = () => {
  let index = 0;
  return {
    next: () => {
      return { value: index++, done: false };
    }
  };
};

test("`next` interface for infinite iterators", () => {
  const iterator = idGenerator();

  expect(iterator.next().value).toEqual(0);
  expect(iterator.next().value).toEqual(1);
});

class FruitIterator {
  constructor(list) {
    this.index = 0;
    this.list = list;
  }

  [Symbol.iterator]() {
    return {
      next: () => {
        if (this.index >= this.list.length) return { done: true };

        return { value: this.list[this.index++], done: false };
      }
    };
  }
}

test("iteration over class with `of` loop", () => {
  const iterator = new FruitIterator(fruits);

  for (const id of iterator) {
    expect(fruits).toContain(id);
  }
});

const fruitAsyncIterator = list => {
  let index = 0;
  return {
    next: () => {
      if (index >= list.length) return Promise.resolve({ done: true });

      return new Promise(resolve => {
        setTimeout(() => {
          resolve({
            value: list[index++],
            done: false
          });
        }, 500);
      });
    }
  };
};

test("async iterators", end => {
  fruitAsyncIterator(fruits)
    .next()
    .then(({ value, done }) => {
      expect(value).toEqual("Mango");
      expect(done).toBe(false);
      end();
    });
});
