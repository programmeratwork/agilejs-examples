describe("synchronous generators", () => {
  function* idGenerator() {
    var index = 0;
    while (true) yield index++;
  }

  test("infinite value generation", () => {
    const iterator = idGenerator();

    expect(iterator.next().value).toEqual(0);
    expect(iterator.next().value).toEqual(1);
  });
});

describe("asynchronous generators", () => {
  const fruits = ["Mango", "Orange"];

  async function* fruitAsyncGenerator(fruits) {
    const resolveFruit = index =>
      new Promise(resolve => {
        setTimeout(() => {
          resolve(fruits[index]);
        }, 500);
      });

    let index = 0;

    while (index < fruits.length) {
      yield await resolveFruit(index);
      index++;
    }
  }

  test("async iterators", async () => {
    for await (const fruit of fruitAsyncGenerator(fruits)) {
      expect(fruits).toContain(fruit);
    }
  });
});
