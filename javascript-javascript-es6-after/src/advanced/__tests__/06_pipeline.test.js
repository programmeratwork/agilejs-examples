test("pipeline operator", () => {
  const double = n => n * 2;
  const increment = n => n + 1;

  const result = 5 |> double |> double |> increment |> double;

  expect(result).toEqual(42);
});
