const ricardo = { login: "borillo", name: "Ricardo Borillo" };

const retrieveUsers = async () => {
  return [ricardo];
};

const retrieveUsersWithError = async () => {
  throw new Error("Can not retrieve users!!");
};

test("an async method returns always a promise", async () => {
  const users = retrieveUsers();

  expect(users).toBeInstanceOf(Promise);
});

test("we don't need `then` with await", async () => {
  const users = await retrieveUsers();

  expect(users).toContainEqual(ricardo);
});

test("exceptions have to be managed with `catch`", async () => {
  try {
    await retrieveUsersWithError();
  } catch (e) {
    expect(e.message).toMatch("Can not retrieve users");
  }
});
