const ricardo = { login: "borillo", name: "Ricardo Borillo" };
const jaime = { login: "perera", name: "Jaime Perera" };

const retrieveUsers = () => {
  return new Promise(resolve => {
    setTimeout(() => resolve([ricardo]), 100);
  });
};

const retrieveSlowUsers = () => {
  return new Promise(resolve => {
    setTimeout(() => resolve([jaime]), 500);
  });
};

const retrieveUsersWithError = () => {
  return new Promise(() => {
    throw Error("No users available");
  });
};

test("an async method returning plain data should fullfile the promise", end => {
  retrieveUsers().then(users => {
    expect(users).toHaveLength(1);
    end();
  });
});

test("an async method throwing and error rejects the promise", end => {
  retrieveUsersWithError().catch(error => {
    expect(error).toEqual(new Error("No users available"));
    end();
  });
});

test("all-or-nothing operation", end => {
  Promise.all([retrieveUsers(), retrieveUsers()]).then(
    ([usersFirst, usersSecond]) => {
      expect(usersFirst).toHaveLength(1);
      expect(usersSecond).toHaveLength(1);
      end();
    }
  );
});

test("all-or-nothing operation failing", end => {
  Promise.all([retrieveUsers(), retrieveUsersWithError()])
    .then(result => {
      throw Error("Never happends!!");
    })
    .catch(error => {
      expect(error).toEqual(new Error("No users available"));
      end();
    });
});

test("race operation", end => {
  Promise.race([retrieveUsers(), retrieveSlowUsers()]).then(result => {
    expect(result).toContainEqual(ricardo);
    end();
  });
});

test("immediate rejected promise", end => {
  Promise.reject("Error!!").catch(error => {
    expect(error).toEqual("Error!!");
    end();
  });
});

test("immediate resolved promise", end => {
  Promise.resolve("It works!!").then(result => {
    expect(result).toEqual("It works!!");
    end();
  });
});

test("`finally` support", end => {
  Promise.resolve("It works!!").finally(() => {
    end();
  });
});
