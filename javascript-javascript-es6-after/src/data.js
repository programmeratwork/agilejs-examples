const films = [
  {
    episode: 1,
    title: "The Phantom Menace",
    year: 1999,
    director: "George Lucas"
  },
  {
    episode: 2,
    title: "Attack of the Clones",
    year: 2002,
    director: "George Lucas"
  },
  {
    episode: 3,
    title: "Revenge of the Sith",
    year: 2005,
    director: "George Lucas"
  },
  {
    episode: 4,
    title: "A New Hope",
    year: 1977,
    director: "George Lucas"
  },
  {
    episode: 5,
    title: "The Empire Strikes Back",
    year: 1980,
    director: "Irvin Kershner"
  },
  {
    episode: 6,
    title: "Return of the Jedi",
    year: 1983,
    director: "Richard Marquand"
  },
  {
    episode: 7,
    title: "The Force Awakens",
    year: 2015,
    director: "J. J. Abrams"
  },
  {
    episode: 8,
    title: "The Last Jedi",
    year: 2017,
    director: "Rian Johnson"
  }
];

export default films;
