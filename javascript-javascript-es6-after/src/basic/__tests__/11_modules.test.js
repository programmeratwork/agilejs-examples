import dep from "../../dependency";

it("import statements", function() {
  expect(dep.value).toEqual(1);
  expect(dep.one()).toEqual("one");
  expect(dep.two()).toEqual("two");
});
