test("can defined variables with let", function() {
  let value = 32;
  expect(value).toEqual(32);
});

test("can define const values", function() {
  const value = 32;
  //value = 23;
  expect(value).toEqual(32);
});

test("variables are block-scoped", function() {
  let value = 32;

  if (true) {
    let value = 23;
  }

  expect(value).toEqual(32);
});
