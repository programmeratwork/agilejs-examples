test("definition and inheritance", function() {
  function OldCar() {
    this.fuel = 10;
  }

  OldCar.prototype.move = function() {
    this.fuel--;
  };

  let es5Car = new OldCar();
  es5Car.move();
  expect(es5Car.fuel).toEqual(9);

  class Car {
    constructor() {
      this.fuel = 10;
    }

    move() {
      this.fuel--;
    }
  }

  let car = new Car();
  car.move();
  expect(car.fuel).toEqual(9);

  class ExpensiveCar extends Car {
    move() {
      this.fuel -= 5;
    }
  }

  let expensiveCar = new ExpensiveCar();
  expensiveCar.move();
  expect(expensiveCar.fuel).toEqual(5);
});
