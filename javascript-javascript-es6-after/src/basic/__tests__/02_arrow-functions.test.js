test("Arrow functions with multiple lines use braces and must define `return`", function() {
  const multiplyByTwo = num => {
    let multiplier = 2;
    return num * multiplier;
  };
  let result = multiplyByTwo(3);

  expect(result).toEqual(6);
});

test("Inline arrow functions don't need braces and `return`", function() {
  const multiplyByTwo = num => num * 2;
  let result = multiplyByTwo(3);

  expect(result).toEqual(6);
});