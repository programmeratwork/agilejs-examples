test("catch arguments are not mandatory", end => {
  try {
    throw Error("💣");
  } catch {
    end();
  }
});
