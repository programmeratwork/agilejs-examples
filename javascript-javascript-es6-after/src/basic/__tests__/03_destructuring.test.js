test("arrays", function() {
  var [a, , b] = [1, 2, 3];

  expect(a).toEqual(1);
  expect(b).toEqual(3);
});

test("objects", function() {
  var user = { name: "Ricardo Borillo", age: 40 };
  var { name, age } = user;

  expect(name).toEqual("Ricardo Borillo");
  expect(age).toEqual(40);
});

test("aliases", function() {
  var user = { name: "Ricardo Borillo", age: 40 };
  var { name: myName, age } = user;

  expect(myName).toEqual("Ricardo Borillo");
});

test("nested destructuring", function() {
  var user = {
    name: "Ricardo Borillo",
    profile: {
      id: 123,
      profileName: "ADMIN",
      rol: { id: 45, rolName: "MANAGER" }
    }
  };

  var {
    profile: {
      profileName,
      rol: { rolName }
    }
  } = user;

  expect(profileName).toEqual("ADMIN");
  expect(rolName).toEqual("MANAGER");
});

test("default values", function() {
  let { foo = 3 } = { foo: 2 };
  expect(foo).toEqual(2);

  var { fooo = 3 } = { bar: 2 };
  expect(fooo).toEqual(3);
});

test("optional parameters", function() {
  const mult = (a = 2, b = 30) => a * b;

  expect(mult()).toEqual(60);
  expect(mult(7, 8)).toEqual(56);
});
