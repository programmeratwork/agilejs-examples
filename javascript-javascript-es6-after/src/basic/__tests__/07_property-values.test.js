test("no need to define prop and value", function() {
  let name = "Ricardo";
  let surname = "Borillo";
  let user = { name, surname };

  expect(user).toEqual({ name: "Ricardo", surname: "Borillo" });
});
