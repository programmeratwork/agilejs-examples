const gurus = [
  {
    alias: "fowler",
    name: "Martin Fowler",
    books: 12,
    gender: "Male"
  },
  {
    alias: "unclebob",
    name: "Robert Martin",
    books: 6,
    gender: "Male"
  },
  {
    alias: "beck",
    name: "Kent Beck",
    books: 8,
    gender: "Male"
  }
];

describe("Array.from", () => {
  test("create an array from an string", () => {
    const greeting = Array.from("hello");

    expect(greeting).toHaveLength(5);
  });

  test("create an array from array-like variables", () => {
    const dummyFunction = () => {
      const arrayDefinition = Array.from(arguments);

      expect(arrayDefinition).toBeInstanceOf(Array);
    };

    dummyFunction();
  });
});

describe("Array.isArray", () => {
  test("detect array type", () => {
    expect(Array.isArray(gurus)).toBe(true);
    expect(Array.isArray({ foo: 123 })).toBe(false);
    expect(Array.isArray("foobar")).toBe(false);
    expect(Array.isArray(undefined)).toBe(false);
  });
});

describe("Array.of", () => {
  test("create an array from input elements", () => {
    expect(Array.of(7)).toEqual([7]);
    expect(Array.of(1, 2, 3)).toEqual([1, 2, 3]);
  });
});

describe("Flatten arrays", () => {
  test("flat only apply to one level deep by default", () => {
    const result = ["apple", ["mango", ["pear"], "melon"]].flat();

    expect(result).toEqual(["apple", "mango", ["pear"], "melon"]);
  });

  test("flat as many levels as you define", () => {
    const result = ["apple", ["mango", ["pear"], "melon"]].flat(2);

    expect(result).toEqual(["apple", "mango", "pear", "melon"]);
  });

  test("Infinity levels to flat can be defined", () => {
    const result = ["apple", ["mango", ["pear"], "melon"]].flat(Infinity);

    expect(result).toEqual(["apple", "mango", "pear", "melon"]);
  });

  test("flat removes array holes", () => {
    const result = ["apple", , , "melon"].flat();

    expect(result).toEqual(["apple", "melon"]);
  });

  test("Apply `map` and flat the result", () => {
    const result = ["lorem ipsum", "value two"].flatMap(value =>
      value.split(" ")
    );

    expect(result).toEqual(["lorem", "ipsum", "value", "two"]);
  });
});

describe("Functional operations", () => {
  test("obtain alias from gurus", () => {
    const alias = gurus.map(guru => guru.alias);

    expect(alias).toEqual(["fowler", "unclebob", "beck"]);
  });

  test("obtain gurus with an alias length > 4 characters", () => {
    const longAliasGurus = gurus.filter(guru => guru.alias.length > 4);

    expect(longAliasGurus).toHaveLength(2);
  });

  test("books written by gurus", () => {
    const booksWritten = gurus
      .map(guru => guru.books)
      .reduce((acumulator, current) => acumulator + current, 0);

    expect(booksWritten).toEqual(26);
  });

  test("find fowler", () => {
    const fowler = gurus.find(guru => guru.alias === "fowler");

    expect(fowler.alias).toEqual("fowler");
  });

  test("find unclebob index in the array", () => {
    const unclebob = gurus.findIndex(guru => guru.alias === "unclebob");

    expect(unclebob).toEqual(1);
  });

  test("some of our gurus are males", () => {
    const haveMales = gurus.some(guru => guru.gender === "Male");
    const haveFemales = gurus.some(guru => guru.gender === "Female");

    expect(haveMales).toBe(true);
    expect(haveFemales).toBe(false);
  });

  test("count gurus", () => {
    let numGurus = 0;

    gurus.forEach(guru => {
      numGurus++;
    });

    expect(numGurus).toEqual(gurus.length);
  });

  test("filter empty or falsy values", () => {
    const result = ["apple", null, , undefined, "melon"].filter(Boolean);

    expect(result).toEqual(["apple", "melon"]);
  });
});
