const bag = { apple: 23, orange: 54, melon: 22 };

test("object `keys`, `values` and `entries`", () => {
  expect(Object.keys(bag)).toEqual(["apple", "orange", "melon"]);
  expect(Object.values(bag)).toEqual([23, 54, 22]);
  expect(Object.entries(bag)).toEqual([
    ["apple", 23],
    ["orange", 54],
    ["melon", 22]
  ]);
});

test("add new values with `assign`", () => {
  const bagPlusKiwi = Object.assign({}, bag, { kiwi: 8 });

  expect(bagPlusKiwi).toEqual({ apple: 23, orange: 54, melon: 22, kiwi: 8 });
});

test("`assign` is equivalent to object spread", () => {
  const bagPlusKiwi = Object.assign({}, bag, { kiwi: 8 });

  expect(bagPlusKiwi).toEqual({ ...bag, kiwi: 8 });
});

test("Create object from `entries` list", () => {
  const bagFromEntries = Object.fromEntries([
    ["apple", 23],
    ["orange", 54],
    ["melon", 22]
  ]);

  expect(bagFromEntries).toEqual(bag);
});
