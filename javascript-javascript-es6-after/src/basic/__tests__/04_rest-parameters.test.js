test("destructuring", function() {
  const list = [1, 2, 3, 4];
  let a = list[0],
    rest = list.slice(1);

  expect(a).toEqual(1);
  expect(rest).toEqual([2, 3, 4]);

  [a, ...rest] = list;
  expect(a).toEqual(1);
  expect(rest).toEqual([2, 3, 4]);
});

test("function array argument", function() {
  const concat = (...words) => {
    return words.join(" ");
  };

  expect(concat("Hola", "Ricardo", "!!")).toEqual("Hola Ricardo !!");
});

test("concatenate arrays", function() {
  expect([1, 2].concat([3, 4])).toEqual([1, 2, 3, 4]);
  expect([1, 2, ...[3, 4]]).toEqual([1, 2, 3, 4]);
});
