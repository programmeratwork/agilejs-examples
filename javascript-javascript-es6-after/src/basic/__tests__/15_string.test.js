describe("padding", () => {
  const stringValue = "value";
  const numericValue = 123;

  test("pad start string with empty spaces", () => {
    const paddedValue = stringValue.padStart(10);

    expect(paddedValue).toEqual(`     ${stringValue}`);
  });

  test("pad number up to 10 digits", () => {
    const paddedValue = String(numericValue).padStart(10, "0");

    expect(paddedValue).toEqual(`0000000${numericValue}`);
  });

  test("pad end string with empty spaces", () => {
    const paddedValue = stringValue.padEnd(10);

    expect(paddedValue).toEqual(`${stringValue}     `);
  });

  test("pad end string with dots up to 10 characters", () => {
    const paddedValue = String(numericValue).padEnd(10, ".");

    expect(paddedValue).toEqual(`${numericValue}.......`);
  });
});

describe("trim", () => {
  test("trim the start of an string", () => {
    const result = "    Hi!  ".trimStart();

    expect(result).toEqual("Hi!  ");
  });

  test("trim the end of an string", () => {
    const result = "  Hi!      ".trimEnd();

    expect(result).toEqual("  Hi!");
  });

  test("trim the whole string", () => {
    const result = "  Hi!      ".trim();

    expect(result).toEqual("Hi!");
  });
});
