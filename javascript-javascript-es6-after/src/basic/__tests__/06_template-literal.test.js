test("all kinds of quotes", function() {
  expect(`I'm "amazed"`).toEqual('I\'m "amazed"');
});

test("multiline", function() {
  let greeting = `Hi
My
Friends`;

  expect(greeting).toEqual("Hi\nMy\nFriends");
});

test("interpolate variables", function() {
  let name = "Ricardo";

  expect(`Hi ${name}`).toEqual("Hi Ricardo");
});

test("support tagged templates", () => {
  const hi = (literals, ...names) => `Hi ${names.join(" and ")}!`;

  const name = "Ricardo";
  const other = "Pedro";
  const result = hi`Nice to see you ${name} and ${other}!`;

  expect(result).toEqual("Hi Ricardo and Pedro!");
});
