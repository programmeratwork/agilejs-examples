import React from "react";

const App = () => {
  const [value, setValue] = [null, null];

  return (
    <div>
      <h1>Current value is {value}</h1>

      <button onClick={() => setValue(value - 1)}>-</button>
      <button onClick={() => setValue(value + 1)}>+</button>
    </div>
  );
};

export default App;
