import React from "react";

import { render, cleanup } from "react-testing-library";

import { Alert } from "../App";

const logger = jest.fn(() => {});

describe("react lifecycle", () => {
  test("new components", () => {
    const { getByText } = render(<Alert type="BLUE" logger={logger} />);

    expect(getByText(/blue alert/i)).toBeDefined();

    expect(logger).toHaveBeenCalledTimes(3);
    expect(logger.mock.calls[0][0]).toEqual("constructor");
    expect(logger.mock.calls[1][0]).toEqual("render");
    expect(logger.mock.calls[2][0]).toEqual("componentDidMount");
  });

  test("removed components", () => {
    render(<Alert type="BLUE" logger={logger} />);

    unmountComponentAndClearMocks();

    expect(logger.mock.calls[0][0]).toEqual("componentWillUnmount");
  });

  test("updated components", () => {
    const { rerender } = render(<Alert type="BLUE" logger={logger} />);
    expect(logger).toHaveBeenCalledTimes(3);

    logger.mockClear();

    rerender(<Alert type="RED" logger={logger} />);

    expect(logger).toHaveBeenCalledTimes(2);
    expect(logger.mock.calls[0][0]).toEqual("render");
    expect(logger.mock.calls[1][0]).toEqual("componentDidUpdate");
  });
});

function unmountComponentAndClearMocks() {
  logger.mockClear();
  cleanup();
}

afterEach(unmountComponentAndClearMocks);
