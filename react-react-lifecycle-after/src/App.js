import React, { Component } from "react";

export class Alert extends Component {
  constructor(props) {
    super(props);

    props.logger("constructor");
  }

  componentDidMount() {
    this.props.logger("componentDidMount");
  }

  componentDidUpdate() {
    this.props.logger("componentDidUpdate");
  }

  componentWillUnmount() {
    this.props.logger("componentWillUnmount");
  }

  render() {
    this.props.logger("render");
    return <div>{`${this.props.type} alert!!`}</div>;
  }
}

function logger(value) {
  console.log(value);
}

class App extends Component {
  state = {
    alertInProgress: false,
    alertType: "BLUE"
  };

  changeAlertProgress = () => {
    this.setState(prevState => ({
      alertInProgress: !prevState.alertInProgress
    }));
  };

  changeAlertType = () => {
    this.setState(prevState => ({
      alertType: prevState.alertType === "RED" ? "BLUE" : "RED"
    }));
  };

  render() {
    const { alertInProgress, alertType } = this.state;

    return (
      <div>
        {alertInProgress && <Alert type={alertType} logger={logger} />}
        <button onClick={this.changeAlertProgress}>
          change alert progress
        </button>
        <button onClick={this.changeAlertType}>change alert type</button>
      </div>
    );
  }
}

export default App;
