import React, { Component } from "react";

class Articles extends Component {
  render() {
    return (
      <div>
        <h2>Articles</h2>

        <article>
          <header>Title 1</header>
          <p>Content 1</p>
        </article>
        <article>
          <header>Title 2</header>
          <p>Content 2</p>
        </article>
      </div>
    );
  }
}

export default Articles;
