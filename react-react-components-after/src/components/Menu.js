import React, { Component } from "react";

class Menu extends Component {
  render() {
    return (
      <aside>
        <h2>Menu</h2>

        <ul>
          <li>
            <a href="/home">Home</a>
          </li>
          <li>
            <a href="/help">Help</a>
          </li>
        </ul>
      </aside>
    );
  }
}

export default Menu;
