import React, { Component } from "react";

import Menu from "./Menu";
import Articles from "./Articles";

class Body extends Component {
  render() {
    return (
      <section>
        <Menu />
        <Articles />
      </section>
    );
  }
}

export default Body;
