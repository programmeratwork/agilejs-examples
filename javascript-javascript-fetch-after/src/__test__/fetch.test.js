import "isomorphic-fetch";

const FILMS_URL = "https://swapi.co/api/films/";
const NUMBER_OF_FILMS = 7;

/*
  Response excerpt:

  {
    "count": 7, 
    "next": null, 
    "previous": null, 
    "results": [
      {
        "title": "A New Hope", 
        "episode_id": 4, 
        ...
      }
    ]
  }
  */

test("should be possible to retrieve remote data with `fetch`", async () => {
  const response = await fetch(FILMS_URL);
  const films = await response.json();

  expect(films.count).toEqual(NUMBER_OF_FILMS);
});
