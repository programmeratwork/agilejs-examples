import React, { Component } from "react";

import { Query } from "react-apollo";
import gql from "graphql-tag";

class App extends Component {
  render() {
    return (
      <Query
        query={gql`
          query {
            pokemons(first: 10) {
              id
              name
              image
            }
          }
        `}
      >
        {({ loading, error, data }) => {
          if (loading) return <p>Loading...</p>;
          if (error) return <p>Error :(</p>;

          return data.pokemons.map(({ id, name, image }) => (
            <div key={id}>
              <div>{name}</div>
              <div>
                <img alt={name} style={{ width: "3rem" }} src={image} />
              </div>
            </div>
          ));
        }}
      </Query>
    );
  }
}

export default App;
