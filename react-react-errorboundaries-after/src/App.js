import React from "react";

import ErrorHandler from "./ErrorHandler";
import BuggyComponent from "./BuggyComponent";

const App = () => (
  <ErrorHandler>
    <BuggyComponent />
  </ErrorHandler>
);

export default App;
