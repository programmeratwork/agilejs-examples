import React, { Component } from "react";

class BuggyComponent extends Component {
  state = { counter: 0 };

  handleClick = () => {
    this.setState(({ counter }) => ({
      counter: counter + 1
    }));
  };

  render() {
    if (this.state.counter === 3) throw new Error("Count overflow!!");

    return (
      <h1 onClick={this.handleClick}>
        Clic me to count!! {this.state.counter} times
      </h1>
    );
  }
}

export default BuggyComponent;
