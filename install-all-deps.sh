#!/bin/bash

for i in $(ls -d */); do
   echo "Installing deps for $i..."

   cd $i
   npm install
   cd -
done
