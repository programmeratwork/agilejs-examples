import sum from "../sum";

expect.extend({
  toBeEven(received) {
    if (received % 2 === 0) {
      return { pass: true };
    }

    return {
      message: () => `expected ${received} to be even`,
      pass: false
    };
  }
});

test("should detect even numbers", () => {
  expect(sum(4, 2)).toBeEven();
  expect(sum(4, 1)).not.toBeEven();
});
