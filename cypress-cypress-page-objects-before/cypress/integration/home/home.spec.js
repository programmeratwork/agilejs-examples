context("Home page", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("should show the app title", () => {
    cy.title().should("eq", "ReactWars");
  });

  it("should show the film list as default info", () => {
    cy.get(".films-panel").contains("Film list");
  });

  it("should show a menu", () => {
    cy.get(".app-menu > li").should(nodeList => {
      expect(nodeList).to.have.length(2);
    });
  });
});
