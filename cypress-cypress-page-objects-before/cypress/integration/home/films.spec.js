context("Films", () => {
  beforeEach(() => {
    cy.server();
    cy.route("/api/films", "fixture:films");

    cy.visit("/films");
  });

  it("should show the episode list", () => {
    cy.get(".film").should("have.length", 8);
  });

  it("should show the episode name and number", () => {
    cy.get(".film .title").contains("La amenaza fantasma");
    cy.get(".film .episode").contains("Episode 1");
  });
});
