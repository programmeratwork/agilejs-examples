import React, { Component } from "react";

const Fuji = props => {
  const { awesome } = props.location.state;
  const { color } = props;

  return (
    <div style={{ color }}>
      <span>I'm a Fuji apple!! </span>
      {awesome && <span>And i'm awesome!!</span>}
    </div>
  );
};

const Granny = ({
  match: {
    params: { id }
  },
  color
}) => {
  return (
    <div style={{ color }}>I'm a Granny Smith apple!! And my ID is {id}</div>
  );
};

const Apple = ({ match, location }) => {
  return (
    <div>
      <h1>Welcome to the apple's world!!</h1>

      <nav>
        <a href="/apple/fuji">Fuji</a>
        {" | "}
        <a href="/apple/granny/99">Granny</a>
      </nav>

      <div>Nested applet types goes here!!</div>
    </div>
  );
};

const Orange = () => <div>No subtypes for orange have been defined :/</div>;

class App extends Component {
  render() {
    return (
      <div>
        <h1>Food Market</h1>

        <nav>
          <a href="/apple">Apple</a>
          {" | "}
          <a href="/orange">Orange</a>
        </nav>

        <div>Content goes here!!</div>
      </div>
    );
  }
}

export default App;
