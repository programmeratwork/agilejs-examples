import React from "react";

import "react-testing-library/cleanup-after-each";

import { render, fireEvent } from "react-testing-library";

import App from "../src/App";

test("assert elements using the label", () => {
  const { getByLabelText } = render(<App />);
  const user = getByLabelText("Username");

  expect(user.value).toEqual("borillo");
});

test("assert elements using placeholder", () => {
  const { getByPlaceholderText } = render(<App />);

  let reason = getByPlaceholderText("Introduce the reason");
  reason.value = "This is a new user";
  fireEvent.change(reason);

  reason = getByPlaceholderText("Introduce the reason");
  expect(reason.value).toEqual("This is a new user");
});

test("assert elements using textual values", () => {
  const { getByText } = render(<App />);
  let title = getByText(/user form/i);

  expect(title.textContent).toEqual("New user form");
});
