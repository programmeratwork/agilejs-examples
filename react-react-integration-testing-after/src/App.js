import React, { Component } from "react";

class App extends Component {
  render() {
    return (
      <div>
        <h1>New user form</h1>

        <form>
          <label htmlFor="username-input">Username</label>
          <input id="username-input" defaultValue="borillo" />

          <label htmlFor="reason-input">Reason</label>
          <input id="reason-input" placeholder="Introduce the reason" />
        </form>
      </div>
    );
  }
}

export default App;
