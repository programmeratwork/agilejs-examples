import React from "react";

import { render, cleanup } from "react-testing-library";

import { Alert } from "../App";

const logger = jest.fn(() => {});

describe("react lifecycle", () => {
  test("new components", () => {
    const { getByText } = render(<Alert type="BLUE" logger={logger} />);

    expect(getByText("TODO")).toBeDefined();

    expect(logger).toHaveBeenCalledTimes(_);
    expect(logger.mock.calls[_][_]).toEqual(_);
  });

  test("removed components", () => {
    render(<Alert type="BLUE" logger={logger} />);

    unmountComponentAndClearMocks();

    expect(logger.mock.calls[_][_]).toEqual(_);
  });

  test("updated components", () => {
    const { rerender } = render(<Alert type="BLUE" logger={logger} />);
    expect(logger).toHaveBeenCalledTimes(_);

    logger.mockClear();

    rerender(<Alert type="RED" logger={logger} />);

    expect(logger).toHaveBeenCalledTimes(_);
    expect(logger.mock.calls[_][_]).toEqual(_);
    expect(logger.mock.calls[_][_]).toEqual(_);
  });
});

function unmountComponentAndClearMocks() {
  logger.mockClear();
  cleanup();
}

afterEach(unmountComponentAndClearMocks);
