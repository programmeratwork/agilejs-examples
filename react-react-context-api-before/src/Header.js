import React, { Fragment } from "react";

import Logo from "./Logo";
import User from "./User";

const Header = () => {
  return (
    <Fragment>
      <Logo />
      <User />
    </Fragment>
  );
};

export default Header;
