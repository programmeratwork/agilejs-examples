import React from "react";

const User = ({ user }) => (
  <div>
    <strong>Logged as:</strong> <span>{user}</span>
  </div>
);

export default User;
