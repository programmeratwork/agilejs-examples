jest.mock(
  "../AuthRepository",
  () =>
    class {
      retrieveCredentials() {
        return Promise.resolve({
          user: "testuser",
          roles: ["ADMIN"]
        });
      }
    }
);

import React from "react";
import { render, waitForElement } from "react-testing-library";

import App from "../App";

test("", async () => {
  const { getByText } = render(<App />);

  const userName = await waitForElement(() => getByText("testuser"));
  expect(userName).toBeDefined();
});
