import React, { Component, Fragment } from "react";
import AuthRepository from "./AuthRepository";

import Header from "./Header";
import Body from "./Body";

class App extends Component {
  state = {
    credentials: {}
  };

  async componentDidMount() {
    const authRepository = new AuthRepository();
    const credentials = await authRepository.retrieveCredentials();

    this.setState({ credentials });
  }

  render() {
    return (
      <Fragment>
        <Header />
        <Body />
      </Fragment>
    );
  }
}

export default App;
