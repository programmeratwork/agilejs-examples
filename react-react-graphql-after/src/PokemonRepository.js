import ApolloClient from "apollo-boost";
import gql from "graphql-tag";

const client = new ApolloClient({
  uri: "https://graphql-pokemon.now.sh/"
});

export default {
  async retrievePokemons() {
    const { data } = await client.query({
      query: gql`
        query {
          pokemons(first: 10) {
            id
            name
            image
          }
        }
      `
    });

    return data.pokemons;
  }
};
