import React, { Component } from "react";

import PokemonRepository from "./PokemonRepository";

class App extends Component {
  state = {
    pokemons: []
  };

  async componentDidMount() {
    const pokemons = await PokemonRepository.retrievePokemons();
    this.setState({ pokemons });
  }

  render() {
    const { pokemons } = this.state;

    return (
      <div>
        {pokemons.map(({ id, name, image }) => (
          <div key={id}>
            <div>{name}</div>
            <div>
              <img alt={name} style={{ width: "3rem" }} src={image} />
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default App;
