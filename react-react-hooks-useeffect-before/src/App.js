import React from "react";

const App = ({ put, remove }) => {
  const [value, setValue] = [null, null];
  const [reset, setReset] = [null, null];

  put("agilejs:app", "HOOKS");
  put("agilejs:app:value", value);
  put("agilejs:app:reset", reset);
  remove("agilejs:app:reset");

  return (
    <div>
      <h1>Current value is {value}</h1>
      <h3>Reset executed {reset} times</h3>

      <button onClick={() => setValue(value - 1)}>-</button>
      <button onClick={() => setValue(value + 1)}>+</button>
      <button
        onClick={() => {
          setValue(0);
          setReset(reset + 1);
        }}
      >
        Reset
      </button>
    </div>
  );
};

export default App;
