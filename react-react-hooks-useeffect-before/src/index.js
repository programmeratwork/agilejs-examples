import React from "react";
import ReactDOM from "react-dom";

import App from "./App";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <App
    put={(key, value) => window.localStorage.setItem(key, value)}
    remove={key => window.localStorage.removeItem(key)}
  />,
  rootElement
);
