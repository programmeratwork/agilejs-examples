import React from "react";
import { render, cleanup, fireEvent } from "react-testing-library";

import App from "../App";

afterEach(cleanup);

const put = jest.fn(() => {});
const remove = jest.fn(() => {});

afterEach(() => {
  cleanup();

  put.mockClear();
  remove.mockClear();
});

test("root key should be stored only once", () => {
  const { rerender } = render(<App put={put} remove={remove} />);

  expect(put).toHaveBeenCalledTimes(_);
  expect(remove).not.toHaveBeenCalled();
  expect(put.mock.calls[0][0]).toEqual(_);
  expect(put.mock.calls[1][0]).toEqual(_);
  expect(put.mock.calls[2][0]).toEqual(_);

  put.mockClear();
  rerender(<App put={put} remove={remove} />);

  expect(put).toHaveBeenCalledTimes(_);
  expect(remove).toHaveBeenCalledTimes(_);
  expect(put.mock.calls[0][0]).toEqual(_);
  expect(put.mock.calls[0][1]).toEqual(_);
});

test("reset", () => {
  const { rerender, getByText } = render(<App put={put} remove={remove} />);

  expect(put).toHaveBeenCalledTimes(_);
  expect(remove).not.toHaveBeenCalled();
  expect(put.mock.calls[0][0]).toEqual(_);
  expect(put.mock.calls[1][0]).toEqual(_);
  expect(put.mock.calls[2][0]).toEqual(_);

  put.mockClear();

  rerender(<App put={put} remove={remove} />);

  const resetButton = getByText(/_/i);

  fireEvent.click(resetButton);

  expect(put).toHaveBeenCalledTimes(3);
  expect(put.mock.calls[0][0]).toEqual(_);
  expect(put.mock.calls[1][0]).toEqual(_);
  expect(put.mock.calls[2][0]).toEqual(_);
});
