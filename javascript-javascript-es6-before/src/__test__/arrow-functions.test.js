test("inline functions", function() {
  const multiplyByTwo = _;
  let result = multiplyByTwo(3);

  expect(result).toEqual(6);
});

test("multiple block lines", function() {
  const multiplyByTwo = num => {
    return _;
  };
  let result = multiplyByTwo(3);

  expect(result).toEqual(6);
});
