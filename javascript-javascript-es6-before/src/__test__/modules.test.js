import dep from "../dependency";

it("import statements", function() {
  expect(dep.value).toEqual(_);
  expect(dep.one()).toEqual(_);
  expect(dep.two()).toEqual(_);
});
