test("definition and inheritance", function() {
  let car = new Car();
  car.move();
  expect(car.fuel).toEqual(9);

  let expensiveCar = new ExpensiveCar(); // should extend Car
  expensiveCar.move();
  expect(expensiveCar.fuel).toEqual(5);
});
