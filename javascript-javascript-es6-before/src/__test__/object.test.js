const bag = { apple: 23, orange: 54, melon: 22 };

test("object `keys`, `values` and `entries`", () => {
  expect(Object.keys(bag)).toEqual(_);
  expect(Object.values(bag)).toEqual(_);
  expect(Object.entries(bag)).toEqual(_);
});

test("add new values with `assign`", () => {
  const bagPlusKiwi = Object.assign({}, bag, { kiwi: 8 });

  expect(bagPlusKiwi).toEqual(_);
});

test("`assign` is equivalent to object spread", () => {
  const bagPlusKiwi = Object.assign({}, bag, { kiwi: 8 });

  expect(bagPlusKiwi).toEqual(_);
});
