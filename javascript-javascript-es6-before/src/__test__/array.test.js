const gurus = [
  {
    alias: "fowler",
    name: "Martin Fowler",
    books: 12,
    gender: "Male"
  },
  {
    alias: "unclebob",
    name: "Robert Martin",
    books: 6,
    gender: "Male"
  },
  {
    alias: "beck",
    name: "Kent Beck",
    books: 8,
    gender: "Male"
  }
];

describe("Array.from", () => {
  test("create an array from an string", () => {
    const greeting = _;

    expect(greeting).toHaveLength(5);
  });

  test("create an array from array-like variables", () => {
    const dummyFunction = () => {
      const arrayDefinition = _;

      expect(arrayDefinition).toBeInstanceOf(Array);
    };

    dummyFunction();
  });
});

describe("Array.isArray", () => {
  test("detect array type", () => {
    expect(Array.isArray(gurus)).toBe(_);
    expect(Array.isArray({ foo: 123 })).toBe(_);
    expect(Array.isArray("foobar")).toBe(_);
    expect(Array.isArray(undefined)).toBe(_);
  });
});

describe("Array.of", () => {
  test("create an array from input elements", () => {
    expect(Array.of(7)).toEqual(_);
    expect(Array.of(1, 2, 3)).toEqual(_);
  });
});

describe("Flatten arrays", () => {
  test("flat only apply to one level deep", () => {
    const result = ["apple", ["mango", ["pear"], "melon"]].flat();

    expect(result).toEqual(_);
  });

  test("flat removes array holes", () => {
    const result = ["apple", , , "melon"].flat();

    expect(result).toEqual(_);
  });

  test("flat `map` result", () => {
    const result = ["lorem ipsum", "value two"].flatMap(value =>
      value.split(" ")
    );

    expect(result).toEqual(_);
  });
});

describe("Functional operations", () => {
  test("obtain alias from gurus", () => {
    const alias = gurus;

    expect(alias).toEqual(["fowler", "unclebob", "beck"]);
  });

  test("obtain gurus with an alias length > 4 characters", () => {
    const longAliasGurus = gurus;

    expect(longAliasGurus).toHaveLength(2);
  });

  test("books written by gurus", () => {
    const booksWritten = gurus;

    expect(booksWritten).toEqual(26);
  });

  test("find fowler", () => {
    const fowler = gurus;

    expect(fowler.alias).toEqual("fowler");
  });

  test("find unclebob index in the array", () => {
    const unclebob = gurus;

    expect(unclebob).toEqual(1);
  });

  test("some of our gurus are males", () => {
    const haveMales = gurus;
    const haveFemales = gurus;

    expect(haveMales).toBe(true);
    expect(haveFemales).toBe(false);
  });

  test("count gurus", () => {
    let numGurus = 0;

    expect(numGurus).toEqual(gurus.length);
  });

  test("filter empty or falsy values", () => {
    const result = ["apple", null, , undefined, "melon"].filter(Boolean);

    expect(result).toEqual(_);
  });
});
