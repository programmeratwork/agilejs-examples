const ricardo = { login: "borillo", name: "Ricardo Borillo" };
const jaime = { login: "perera", name: "Jaime Perera" };

const retrieveUsers = () => {
  return new Promise(resolve => {
    setTimeout(() => resolve([ricardo]), 100);
  });
};

const retrieveSlowUsers = () => {
  return new Promise(resolve => {
    setTimeout(() => resolve([jaime]), 500);
  });
};

const retrieveUsersWithError = () => {
  return new Promise(() => {
    throw Error("No users available");
  });
};

test("an async method returning plain data should fullfile the promise", end => {
  retrieveUsers().then(users => {
    expect(users).toHaveLength(_);
    end();
  });
});

test("an async method throwing and error rejects the promise", end => {
  retrieveUsersWithError().catch(error => {
    expect(error).toEqual(_);
    end();
  });
});

test("all-or-nothing operation", end => {
  Promise.all([retrieveUsers(), retrieveUsers()]).then(
    ([usersFirst, usersSecond]) => {
      expect(usersFirst).toHaveLength(_);
      expect(usersSecond).toHaveLength(_);
      end();
    }
  );
});

test("all-or-nothing operation failing", end => {
  Promise.all([retrieveUsers(), retrieveUsersWithError()])
    .then(result => {
      throw Error("Never happends!!");
    })
    .catch(error => {
      expect(error).toEqual(_);
      end();
    });
});

test("race operation", end => {
  Promise.race([retrieveUsers(), retrieveSlowUsers()]).then(result => {
    expect(result).toContainEqual(_);
    end();
  });
});

test("immediate rejected promise", end => {
  Promise.reject("Error!!").catch(error => {
    expect(error).toEqual(_);
    end();
  });
});

test("immediate resolved promise", end => {
  Promise.resolve("It works!!").then(result => {
    expect(result).toEqual(_);
    end();
  });
});

test("`finally` support", end => {
  Promise.resolve("It works!!").finally(() => {});
});
