const user = { login: "borillo" };

test("proxy define and object as a result", () => {
  const proxy = new Proxy({}, {});

  expect(proxy).toBeInstanceOf(_);
});

test("`get` can handle existing and not existing properties", () => {
  const proxy = new Proxy(user, {
    get: (target, name) => {
      if (name && target && target[name]) return target[name];

      return "We can handle not existing properties";
    }
  });

  expect(proxy.login).toEqual(_);
  expect(proxy.notExistingProperty).toEqual(_);
});

test("`set` can store values on existing and not existing properties", () => {
  const proxy = new Proxy(user, {
    set: (target, name, value) => {
      if (name === "fullName") {
        target[name] = `The great ${value}!!`;
        return true;
      }

      return false;
    }
  });

  expect(proxy.fullName).toBeUndefined();

  proxy.fullName = "Ricardo Borillo";
  expect(proxy.fullName).toEqual(_);
});

test("`has` can detect existing properties", () => {
  const proxy = new Proxy(user, {
    has: (target, key) => {
      return key === "fullName";
    }
  });

  expect("fullName" in proxy).toBe(_);
  expect("nonManagedProperty" in proxy).toBe(_);
});

test("`apply` within a proxy can decorate functions", () => {
  const operationToDecorate = (a, b) => a * b;
  const decoratedFunction = new Proxy(operationToDecorate, {
    apply: (target, currentThis, args) => {
      return target.apply(currentThis, args) * 10;
    }
  });

  expect(decoratedFunction(2, 2)).toEqual(_);
});

test("`construct` allow to decorate class construction with `new`", () => {
  function User(login) {
    this.login = login;
  }

  const DecoratedClass = new Proxy(User, {
    construct(target, args) {
      return new target(`${args}@programmeratwork.com`);
    }
  });

  expect(new DecoratedClass("borillo").login).toEqual(_);
});

test("deleteProperty", () => {
  const proxy = new Proxy(user, {
    deleteProperty(target, prop) {
      if (prop in target) {
        delete target[prop];
        return true;
      }

      return false;
    }
  });

  expect(proxy.login).toBeDefined();

  delete proxy.login;

  expect(proxy.login).toBeDefined();
});
