test("destructuring", function() {
  const list = [1, 2, 3, 4];
  let a = list[0],
    rest = list.slice(1);

  expect(a).toEqual(_);
  expect(rest).toEqual(_);

  [a, ...rest] = list;
  expect(a).toEqual(_);
  expect(rest).toEqual(_);
});

test("function array argument", function() {
  const concat = (...words) => {
    return words.join(" ");
  };

  expect(concat("Hola", "Ricardo", "!!")).toEqual(_);
});

test("concatenate arrays", function() {
  expect([1, 2].concat([3, 4])).toEqual(_);
  expect([1, 2, ...[3, 4]]).toEqual(_);
});
