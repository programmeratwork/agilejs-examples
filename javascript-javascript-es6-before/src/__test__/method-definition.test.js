test("methods", function() {
  let user = {
    _name: "Ricardo",
    show() {
      return this._name;
    }
  };

  expect(user.show()).toEqual(_);
});

test("getters and setters", function() {
  let user = {
    _name: "Not defined",
    get name() {
      return this._name;
    },
    set name(name) {
      this._name = name;
    }
  };

  user.name = "Ricardo";
  expect(user._name).toEqual(_);
});
