test("arrays", function() {
  var [a, , b] = [1, 2, 3];

  expect(a).toEqual(_);
  expect(b).toEqual(_);
});

test("objects", function() {
  var user = { name: "Ricardo Borillo", age: 40 };
  var { name, age } = user;

  expect(name).toEqual(_);
  expect(age).toEqual(_);
});

test("aliases", function() {
  var user = { name: "Ricardo Borillo", age: 40 };
  var { name: myName, age } = user;

  expect(myName).toEqual(_);
});

test("nested destructuring", function() {
  var user = {
    name: "Ricardo Borillo",
    profile: {
      id: 123,
      profileName: "ADMIN",
      rol: { id: 45, rolName: "MANAGER" }
    }
  };

  var {
    profile: {
      profileName,
      rol: { rolName }
    }
  } = user;

  expect(profileName).toEqual(_);
  expect(rolName).toEqual(_);
});

test("default values", function() {
  let { foo = 3 } = { foo: 2 };
  expect(foo).toEqual(_);

  var { fooo = 3 } = { bar: 2 };
  expect(fooo).toEqual(_);
});

test("optional parameters", function() {
  const mult = (a = 2, b = 30) => a * b;

  expect(mult()).toEqual(_);
  expect(mult(7, 8)).toEqual(_);
});
