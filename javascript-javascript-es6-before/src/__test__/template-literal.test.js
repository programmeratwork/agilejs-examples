test("all kinds of quotes", () => {
  expect('I\'m "amazed"').toEqual(_);
});

test("multiline", () => {
  let greeting = "Hi\nMy\nFriends";

  expect(greeting).toEqual(_);
});

test("interpolate variables", () => {
  let name = "Ricardo";

  expect("Hi " + name).toEqual(_);
});

test("support tagged templates", () => {
  const hi = (literals, ...names) => `Hi ${names.join(" and ")}!`;

  const name = "Ricardo";
  const other = "Pedro";
  const result = hi`Nice to see you ${name} and ${other}!`;

  expect(result).toEqual(_);
});
