test("can defined variables with let", function() {
  let value = 32;
  expect(value).toEqual(_);
});

test("can define const values", function() {
  const value = 32;
  //value = 23;
  expect(value).toEqual(_);
});

test("variables are block-scoped", function() {
  var nonScopedValue = 32;
  let value = 32;

  if (true) {
    var nonScopedValue = 23;
    let value = 23;
  }

  expect(nonScopedValue).toEqual(_);
  expect(value).toEqual(_);
});
