const ricardo = { login: "borillo", name: "Ricardo Borillo" };

const retrieveUsers = () => {
  return new Promise(resolve => {
    resolve([ricardo]);
  });
};

const retrieveUsersWithError = async () => {
  throw new Error("Can not retrieve users!!");
};

test("an async method returns always a promise", async () => {
  const users = retrieveUsers();

  expect(users).toBeInstanceOf(_);
});

test("we don't need `then` with await", async () => {
  const users = retrieveUsers();

  expect(users).toContainEqual(ricardo);
});

test("exceptions have to be managed with `catch`", async () => {
  try {
    retrieveUsersWithError();
  } catch (e) {
    expect(e.message).toMatch(_);
  }
});
