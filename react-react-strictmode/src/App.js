import React, { Component, StrictMode } from "react";

class Header extends Component {
  componentWillMount() {
    console.log('Utilizando un lifecycle method "legacy" :)');
  }
  render() {
    return <div>Header</div>;
  }
}

class Body extends Component {
  executeAction = () => {
    const input = this.refs["value-input"];
    console.log(input.value);
  };

  render() {
    return (
      <div>
        <input
          ref="value-input"
          type="text"
          defaultValue="valor"
          onClick={this.executeAction}
        />
      </div>
    );
  }
}

class App extends Component {
  render() {
    return (
      <StrictMode>
        <Header />
        <Body />
      </StrictMode>
    );
  }
}

export default App;
