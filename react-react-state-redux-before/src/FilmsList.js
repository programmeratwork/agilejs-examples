import React from "react";

const FilmsList = ({ films }) => (
  <div>
    {films.map(({ episode, name }) => (
      <div key={episode}>
        <span>{episode}</span>
        <span>{name}</span>
      </div>
    ))}
  </div>
);

export default FilmsList;
