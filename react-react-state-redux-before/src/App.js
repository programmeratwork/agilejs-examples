import React, { Component } from "react";

import Toolbar from "./Toolbar";
import FilmsList from "./FilmsList";
import FilmsRepository from "./FilmsRepository";

class App extends Component {
  state = {
    films: []
  };

  onLoadData = async () => {
    const films = await FilmsRepository.retrieveFilms();

    this.setState({ films });
  };

  render() {
    const { films } = this.state;

    return (
      <div>
        <Toolbar onLoadData={this.onLoadData} />
        <FilmsList films={films} />
      </div>
    );
  }
}

export default App;
