import React from "react";

const Toolbar = ({ onLoadData }) => {
  return (
    <div>
      <button onClick={onLoadData}>Click to load data</button>
    </div>
  );
};

export default Toolbar;
