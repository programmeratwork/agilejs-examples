import { FILMS_LOAD_START, FILMS_LOAD_END } from "./types";

const defaultState = {
  films: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case FILMS_LOAD_START:
      return;
    case FILMS_LOAD_END:
      return;
    default:
      return state;
  }
};
