import { FILMS_LOAD_START, FILMS_LOAD_END } from "./types";
import repository from "../FilmsRepository";

export const loadFilmsAction = () => {
  return async dispatch => {
    const films = await repository.retrieveFilms();
  };
};
