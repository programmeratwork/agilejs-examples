import React, { Component, Fragment } from "react";

import FilmsRepository from "./FilmsRepository";

const Film = ({ episode, title, year }) => (
  <div>
    {episode} - {title} - {year}
  </div>
);

class Films extends Component {
  state = {
    films: []
  };

  render() {
    if (this.state.films.length === 0) return "No films available!!!";

    return (
      <Fragment>
        {this.state.films.map(film => (
          <Film {...film} />
        ))}
      </Fragment>
    );
  }
}

export default Films;
