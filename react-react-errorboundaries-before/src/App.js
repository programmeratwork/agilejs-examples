import React from "react";

import BuggyComponent from "./BuggyComponent";

const App = () => <BuggyComponent />;

export default App;
