import { FILMS_LOAD_START, FILMS_LOAD_END } from "./types";

const defaultState = {
  films: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case FILMS_LOAD_START:
      return { ...state, loading: true, films: [] };
    case FILMS_LOAD_END:
      return { ...state, loading: false, films: [...action.payload] };
    default:
      return state;
  }
};
