import React from "react";

import Toolbar from "./Toolbar";
import FilmsList from "./FilmsList";

const App = () => {
  return (
    <div>
      <Toolbar />
      <FilmsList />
    </div>
  );
};

export default App;
