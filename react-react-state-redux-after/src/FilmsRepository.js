export default {
  retrieveFilms() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve([
          { episode: 1, name: "The Phantom Menace" },
          { episode: 2, name: "Attack of the Clones" }
        ]);
      }, 2000);
    });
  }
};
