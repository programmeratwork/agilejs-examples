import React from "react";
import { connect } from "react-redux";

const mapStateToProps = data => data;

const FilmsList = ({ loading, films }) => (
  <div>
    {loading && <span>Loading...</span>}

    {films.map(({ episode, name }) => (
      <div key={episode}>
        <span>{episode}</span>
        <span>{name}</span>
      </div>
    ))}
  </div>
);

export default connect(mapStateToProps)(FilmsList);
