import React from "react";
import { connect } from "react-redux";

import { loadFilmsAction } from "./actions/creators";

const mapDispatchToProps = dispatch => {
  return {
    loadFilms: () => {
      dispatch(loadFilmsAction());
    }
  };
};

const Toolbar = ({ loadFilms }) => {
  return (
    <div>
      <button onClick={loadFilms}>Click to load data</button>
    </div>
  );
};

export default connect(
  null,
  mapDispatchToProps
)(Toolbar);
