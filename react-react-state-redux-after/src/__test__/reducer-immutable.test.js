const ADD_GURU = "ADD_GURU";
const REMOVE_GURU = "REMOVE_GURU";
const CHANGE_GURU = "CHANGE_GURU";

const defaultState = {
  gurus: [
    {
      alias: "fowler",
      name: "Martin Fowler"
    },
    {
      alias: "unclebob",
      name: "Robert Martin"
    },
    {
      alias: "beck",
      name: "Kent Beck"
    }
  ]
};

const gurusReducer = (state = defaultState, action) => {
  const { type } = action;

  if (type === ADD_GURU) {
    return {
      gurus: [...state.gurus, action.payload]
    };
  }

  if (type === REMOVE_GURU) {
    const position = state.gurus.findIndex(
      user => user.alias === action.payload
    );

    return {
      gurus: [
        ...state.gurus.slice(0, position),
        ...state.gurus.slice(position + 1)
      ]
    };
  }

  if (type === CHANGE_GURU) {
    const position = state.gurus.findIndex(
      user => user.alias === action.payload.alias
    );

    return {
      gurus: [
        ...state.gurus.slice(0, position),
        action.payload,
        ...state.gurus.slice(position + 1)
      ]
    };
  }

  return state;
};

test("add new guru to state", () => {
  const action = {
    type: ADD_GURU,
    payload: {
      alias: "supercrafter",
      name: "JavaScript Ninja"
    }
  };

  const newState = gurusReducer(defaultState, action);

  expect(newState.gurus).toHaveLength(4);
});

test("remove guru from state", () => {
  const action = {
    type: REMOVE_GURU,
    payload: "unclebob"
  };

  const newState = gurusReducer(defaultState, action);

  expect(newState.gurus).toHaveLength(2);
  expect(newState.gurus).toEqual([
    {
      alias: "fowler",
      name: "Martin Fowler"
    },
    {
      alias: "beck",
      name: "Kent Beck"
    }
  ]);
});

test("change guru name within state", () => {
  const action = {
    type: CHANGE_GURU,
    payload: {
      alias: "unclebob",
      name: "Robert C. Martin"
    }
  };

  const newState = gurusReducer(defaultState, action);
  const unclebob = newState.gurus.find(guru => guru.alias === "unclebob");

  expect(newState.gurus).toHaveLength(3);
  expect(unclebob).toEqual({
    alias: "unclebob",
    name: "Robert C. Martin"
  });
});
